.. doxygen_test documentation master file, created by
   sphinx-quickstart on Wed Jan 10 12:24:12 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to doxygen_test's documentation!
========================================

.. toctree::
   :maxdepth: 3

.. doxygenclass:: configEval
  :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
